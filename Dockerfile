FROM node:8.2.1

WORKDIR /workspace

COPY package.json .
COPY package-lock.json .
RUN npm install

COPY . .

EXPOSE 8080
CMD [ "npm", "start" ]