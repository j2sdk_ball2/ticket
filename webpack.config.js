﻿const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const sourcePath = path.resolve(__dirname, 'src')

const config = {
    context: sourcePath,
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [

            {
                test: /\.js$/,
                include: sourcePath,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: [
                                ['react'],
                                ['es2015', { modules: false }],
                                ['stage-2'],
                                ['stage-3']
                            ]
                        }
                    }
                ]
            },

            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },

            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                    'url-loader?limit=10000',
                    'img-loader'
                ]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'index.html',
            chunks: ['main']
        })
    ],
    devServer: {
        historyApiFallback: true
    }
}

module.exports = config