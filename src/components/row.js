import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Seat from './seat'

export default class Row extends Component {

    render() {
        const { 
            hidden, 
            code, 
            seats 
        } = this.props.row

        if (hidden) {
            return (
                <div className="row-space" >
                </div>
            )
        }

        return (
            <div className="row-container" >
                <div className="row-block letter-container">{code}</div>
                <Seat seats={seats} />
                <div className="row-block letter-container">{code}</div>
            </div>
        )
    }
}