import React, { Component } from 'react'
import ReactDOM from 'react-dom'

export default class Seat extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        const { seats } = this.props
        console.log(`seats: ${JSON.stringify(seats)}`)

        return (
            <div>

                {seats.map((seat, seatIndex) => {

                    const { type } = seat
                    let useTag

                    switch (type) {
                        case 'available-double':
                            useTag = '<use xlink:href="#icon-seat-available" />'
                            return (
                                <svg key={seatIndex}
                                    className="icon icon-double icon-seat-available-double icon-seat-available-extension"
                                    dangerouslySetInnerHTML={{ __html: useTag }}
                                >
                                </svg>
                            )
                            break
                        default:
                            useTag = '<use xlink:href="#icon-seat-available" />'
                            return (
                                <svg key={seatIndex}
                                    className="icon icon-seat-available icon-seat-available-extension"
                                    dangerouslySetInnerHTML={{ __html: useTag }}
                                >
                                </svg>
                            )
                    }

                })}

            </div>
        )
    }
}