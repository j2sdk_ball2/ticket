import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Row from './row'
import Seat from './seat'
import '../../assets/css/styles.css'

export default class Cinema extends Component {

    constructor(props) {
        super(props)
        this.state = {
            areas: [
                {
                    name: 'Deluxe',
                    rows: [
                        {
                            hidden: false,
                            code: 'I',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: false,
                            code: 'H',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                    ]
                },
                {
                    name: 'Premium',
                    rows: [
                        {
                            hidden: false,
                            code: 'G',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: false,
                            code: 'F',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: true,
                            code: 'E',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: false,
                            code: 'D',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: false,
                            code: 'C',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                    ]
                },
                {
                    name: 'Sofa',
                    rows: [
                        {
                            hidden: true,
                            code: 'B',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                        {
                            hidden: false,
                            code: 'A',
                            seats: [
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                                {
                                    type: 'available',
                                },
                            ],
                        },
                    ]
                },
            ]
        }
    }

    render() {
        const { areas } = this.state

        return (
            <div id="container" name="container">

                <div>Ticket App v1</div>
                {areas.map((area, areaIndex) => {

                    const { name, rows } = area
                    const { id } = `area-${name}`

                    return (

                        <div key={areaIndex} id={id} name={id} >
                            {name}

                            {rows.map((row, rowIndex) => {
                                return <Row key={rowIndex} row={row}/>
                            })}

                        </div>
                    )

                })}

            </div>
        )
    }
}