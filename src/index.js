import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import Cinema from './components/cinema'

ReactDOM.render(
    <Cinema />,
    document.getElementById('app')
)